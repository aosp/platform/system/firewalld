// Copyright 2016 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <base/logging.h>
#include <firewalld/firewall.h>

#include <unistd.h>

#include "android/firewalld/IFirewall.h"

#define FIREWALL_BINDER_CLIENT_EXPORT __attribute__((__visibility__("default")))

using std::unique_ptr;
using std::vector;
using std::string;

using android::firewalld::IFirewall;
using android::IBinder;
using android::sp;

namespace firewalld {

const char kServiceName[] = "android.firewalld.Firewall";

class FirewallImpl : public Firewall {
 public:
  explicit FirewallImpl(sp<IBinder> server_firewall)
    : server_firewall_(android::interface_cast<IFirewall>(server_firewall)) {}
  virtual ~FirewallImpl() = default;

  bool PunchTcpHole(int16_t port, const string& iface) override {
    return server_firewall_->PunchTcpHole(port, iface).isOk();
  }

  bool PunchUdpHole(int16_t port, const string& iface) override {
    return server_firewall_->PunchUdpHole(port, iface).isOk();
  }

  bool PlugTcpHole(int16_t port, const string& iface) override {
    return server_firewall_->PlugTcpHole(port, iface).isOk();
  }

  bool PlugUdpHole(int16_t port, const string& iface) override {
    return server_firewall_->PlugUdpHole(port, iface).isOk();
  }

  bool RequestVpnSetup(const vector<string>& usernames,
                       const string& iface) override {
    return server_firewall_->RequestVpnSetup(usernames, iface).isOk();
  }

  bool RemoveVpnSetup(const vector<string>& usernames,
                      const string& iface) override {
    return server_firewall_->RemoveVpnSetup(usernames, iface).isOk();
  }

 private:
  sp<IFirewall> server_firewall_;

  DISALLOW_COPY_AND_ASSIGN(FirewallImpl);
};

FIREWALL_BINDER_CLIENT_EXPORT
unique_ptr<Firewall> Firewall::Connect(
    android::BinderWrapper* binder_wrapper) {
  sp<IBinder> binder(nullptr);
  int retries = 15;

  while (retries--) {
    binder = binder_wrapper->GetService(kServiceName);

    if (binder.get()) {
      break;
    }

    sleep(1);
  }

  if (!binder.get()) {
      LOG(WARNING) << "Could not connect to " << kServiceName << " service.";
    return unique_ptr<Firewall>();
  }

  return unique_ptr<Firewall>(new FirewallImpl(binder));
}

}  // namespace firewalld
