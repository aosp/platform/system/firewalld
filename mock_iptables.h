// Copyright 2015 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef FIREWALLD_MOCK_IPTABLES_H_
#define FIREWALLD_MOCK_IPTABLES_H_

#include <string>

#include <base/macros.h>
#include <gmock/gmock.h>

#include "iptables.h"

namespace firewalld {

class MockIpTables : public IpTables {
 public:
  MockIpTables() = default;
  ~MockIpTables() override;

  int SetRunInMinijailFailCriterion(
      const std::vector<std::string>& keywords, bool repeat, bool omit_failure);
  int GetRunInMinijailCriterionMatchCount(int id);
  bool CheckCommandsUndone();

  // Check if the current command matches a failure rule,
  // if the failure rule is not a repeat rule, remove it
  // from the match criteria.
  bool MatchAndUpdate(const std::vector<std::string>& argv);

 private:
  DISALLOW_COPY_AND_ASSIGN(MockIpTables);

  struct Criterion {
    std::vector<std::string> keywords;
    // If false, remove the criterion after it's matched.
    bool repeat;
    // If false, treat matching commands as failures, otherwise,
    // omit failing.
    bool omit_failure;
    // Count the number of times the criterion is matched.
    int match_count;

    // Don't take into account match_count.
    bool operator==(const Criterion& c) const {
      return (std::equal(keywords.begin(), keywords.end(),
                         c.keywords.begin()) &&
              (repeat == c.repeat) &&
              (c.omit_failure == omit_failure));
    }
  };

  // A list of collections of keywords that a command
  // must have in order for it to fail.
  std::vector<Criterion> match_criteria_;

  // A log of commands issued with RunInMinijail during the test.
  std::vector<std::vector<std::string>> commands_;

  // The mock's implementation simply logs the command issued if
  // successful.
  int RunInMinijail(const std::vector<std::string>& argv) override;

  // Given an ip/iptables command, return the command that undoes it's effect.
  std::vector<std::string> GetInverseCommand(const std::vector<std::string>& command);
};

}  // namespace firewalld

#endif  // FIREWALLD_MOCK_IPTABLES_H_
